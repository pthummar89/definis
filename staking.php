<?php include('header.php'); ?>
      <section class="hero-service">
         <div class="bg-media">
            <img src="images/slider-dot.png" alt="">
         </div>
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                  <div class="hero-service-info">
                     <div class="page-title text-left">
                        <h4>DEFINIS Staking</h4>
                        <h1>The Digital Future of Staking</h1>
                        <p>DEFINIS has a feature of staking to let investors hold crypto assets.</p>
                     </div>
                     <div class="hero-btn">
                        <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                  <div class="hero-service-media text-center">
                     <!-- <img src="images/future.png" alt=""> -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-grid page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>DEFINIS Has Taken Crypto Staking to Another Level</h2>
                     <p>Staking refers to locking crypto assets for a set of the time period to help the operation of the blockchain. DEFINIS allows the staking of crypto assets to crypto holders to have a chance of earning more crypto.</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/staking/secure-staking.png">
                     </div>
                     <h3 class="theme-title">Secure staking</h3>
                     <p class="theme-description">DEFINIS users can securely lock crypto assets for a time period to support the operation of the respective blockchain.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/staking/continuous-new-additions.png">
                     </div>
                     <h3 class="theme-title">Continuous new additions</h3>
                     <p class="theme-description">DEFINIS smartly analyzes the market and we will introduce the reforms that will enhance the experience of staking on DEFINIS.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/staking/flexible-delegation.png">
                     </div>
                     <h3 class="theme-title">Flexible delegation</h3>
                     <p class="theme-description">PoS protocols have the authority to delegate the digital assets on DEFINIS to help them conduct PoS validation.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/staking/automatic-re-delegation.png">
                     </div>
                     <h3 class="theme-title">Automatic Re-Delegation</h3>
                     <p class="theme-description">PoS protocols can automatically re-delegate the digital assets on DEFINIS to reduce human efforts in PoS validation.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/staking/self-serve-delegation.png">
                     </div>
                     <h3 class="theme-title">Self-serve delegation</h3>
                     <p class="theme-description">One or multiple transactions can be delegated by users to another person in a single delegation request on DEFINIS.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/staking/detailed-reporting.png">
                     </div>
                     <h3 class="theme-title">Detailed reporting</h3>
                     <p class="theme-description">DEFINIS provides the detailed reporting of staking rewards on tax returns as per existing rules to DEFINIS users.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-video page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Ready to learn more?</h2>
                     <p>Jump into DEFINIS’ ecosystem by <br>clicking the button below.</p>
                  </div>
                  <div class="service-btn text-center mt-4">
                     <a class="btn-main get-started" href="https://app.definis.io/register">GET STARTED</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>