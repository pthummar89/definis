<footer class="footer-area">
   <div class="footer-top">
      <div class="container">
         <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
               <div class="footer-widget">
                  <div class="footer-logo">
                     <a href=""><img src="images/footer-logo.png" alt=""></a>
                  </div>
                  <p class="theme-description">DEFINIS provides a seamless digital </br>financial experience to investors with </br>an accessible approach in </br>crypto markets.</p>
               </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
               <div class="footer-widget">
                  <div class="footer-title">
                     <h3>Company</h3>
                  </div>
                  <div class="footer-link">
                     <ul>
                        <li><a href="<?php echo url(''); ?>">Home</a></li>
                        <li><a href="<?php echo url('/digital-asset-organizations'); ?>">DEFINIS Digital Solutions</a></li>
                        <li><a href="<?php echo url('/financial-institutions'); ?>">Who We Serve</a></li>
                        <li><a href="<?php echo url('/about'); ?>">About Us</a></li>
                        <li><a class="get-started" href="https://app.definis.io/register">Get Started</a></li>
                        <li><a class="login-text" href="https://app.definis.io/login">Login</a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
               <div class="footer-widget">
                  <div class="footer-title">
                     <h3>For Newsletter</h3>
                  </div>
                  <div class="footer-block">
                     <div class="form-group">
                        <input type="email" class="form-control" placeholder="Enter Email Address">
                        <button class="form-btn">SUBCRIBE</button>
                     </div>
                  </div>
                  <div class="footer-social">
                     <div class="footer-title">
                        <h3>Social Media</h3>
                     </div>
                     <div class="social-ico">
                        <a target="_blank" href="https://twitter.com/definis_web3"><i class="fab fa-twitter"></i></a>
                        <a target="_blank" href="https://www.linkedin.com/company/definis-io/"><i class="fab fa-linkedin-in"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="footer-copyright">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
               <div class="fo-bottom-link">
                  <a href="<?php echo url('/privacy'); ?>">Privacy Policy</a><a href="<?php echo url('/terms'); ?>">Terms of Use</a>
               </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
               <div class="footer-text">
                  <p class="theme-description">Copyright @ 2023 DEFINIS. All rights reserve</p>
               </div>
            </div>
         </div>
      </div>
   </div>

</footer>
<div class="login-main login-popup" id="login-div">
   <div class="table">
      <div class="table-cell">
         <div class="login-close">
            <i class="fas fa-times-circle"></i>
         </div>
         <div class="login-box d-none" id="login-box">
            <div class="login-logo text-center">
               <a href=""><img src="images/footer-logo.png"></a>
            </div>
            <div class="page-title">
               <h2 class="mb-4">Member Login</h2>
            </div>
            <div class="login-form" id="login-popup">
               <form>
                  <div class="form-group">
                     <input type="email" class="form-control" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                     <input type="password" class="form-control" placeholder="Enter Password">
                  </div>
                  <div class="login-text">
                     <p><a class="btn-main" href="">Login</a></p>
                     <p><a href="javascript:void(0)" class="forgot-pass-link">Forgot Password?</a></p>
                     <br>
                     <p>Not a member?</p>
                     <p><a href="javascript:void(0)" class="signup-link">Please register here</a></p>
                  </div>
               </form>
            </div>
         </div>
         <div class="login-box d-none" id="signup-box">
            <div class="login-logo text-center">
               <a href=""><img src="images/footer-logo.png"></a>
            </div>
            <div class="page-title">
               <h2 class="mb-4">Account Creation</h2>
            </div>
            <div class="login-form">
               <form>
                  <div class="form-group">
                     <input type="email" class="form-control" placeholder="Email address">
                  </div>
                  <div class="form-group">
                     <input type="password" class="form-control" placeholder="Enter Password">
                  </div>
                  <div class="form-group">
                     <input type="password" class="form-control" placeholder="Enter Confirm Password">
                  </div>
                  <div class="form-group">
                     <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                        <label class="custom-control-label" for="customCheck1">I am over 18 years old & I have read and agree to the Terms of Service.</label>
                     </div>
                  </div>
                  <div class="login-text">
                     <p><a class="btn-main" href="javascript:void(0)">Continue</a></p>
                     <p>Already registered?</p>
                     <p><a href="javascript:void(0)" class="login-link">Log in</a></p>
                  </div>
               </form>
            </div>
         </div>
         <div class="login-box d-none" id="forgot-pass-box">
            <div class="login-logo text-center">
               <a href=""><img src="images/footer-logo.png"></a>
            </div>
            <div class="page-title">
               <h2 class="mb-4">Forgot Password</h2>
            </div>
            <div class="login-form">
               <form>
                  <div class="form-group">
                     <input type="email" class="form-control" placeholder="Email address">
                  </div>
                  <div class="login-text">
                     <p><a class="btn-main" href="javascript:void(0)">Submit</a></p>
                     <p><a href="javascript:void(0)" class="login-link">Log in</a></p>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<a class="arrow-up" id="arrow-top"><i class="fas fa-arrow-up"></i></a>
<!-- Javascript Files
         ================================================== -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
<script>


</script>
</body>

</html>