<?php include('header.php'); ?>
      <section class="hero-service financial-section">
         <div class="bg-media">
            <img src="images/slider-dot.png" alt="">
         </div>
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                  <div class="hero-service-info">
                     <div class="page-title text-left">
                        <h4>For Digital Asset Organizations</h4>
                        <h1>Secure Your Digital Asset Business</h1>
                        <p>Digital asset organizations can secure their digital asset businesses with the help of the features like custody-execution services on DEFINIS.</p>
                     </div>
                     <div class="hero-btn">
                        <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                  <div class="hero-service-media text-center">
                     <!-- <img src="images/future.png" alt=""> -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="who-we-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Potential Service Acceptors</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="finacing-media text-center mt-4">
                     <img src="images/who-we-serve.svg" alt="">
                  </div>
                  <div class="finacing-media responsive-media text-center mt-4">
                     <img src="images/who-we-serve-responsive.svg" alt="">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="finance-area page-background">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                  <div class="finance-media">
                     <img src="images/screenshots1.png" alt="">
                  </div>
               </div>
               <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                  <div class="finance-info">
                     <div class="page-title text-left">
                        <h2>A Bank-Grade Platform For Managing Digital Assets</h2>
                     </div>
                     <ul>
                        <li>Auditability</li>
                        <li>Sub-Accounts</li>
                        <li>Bank-Grade Workflows</li>
                        <li>Multi-Wallet Infrastructure</li>
                        <li>Transaction Policies</li>
                        <li>Integrated Compliance</li>
                        <li>Connectivity Interfaces</li>
                        <li>Market-Leading Security</li>
                     </ul>
                     <div class="finance-btn">
                        <a href="" class="btn-main">See more</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Digital Asset Services</h2>
                     <p>DEFINIS offers full-fledged services connected to the wider digital asset ecosystem. From custody and DeFi to brokerage, DEFINIS Trust offers a wide range of digital asset services and solutions supported by dedicated customer service available 24x7.</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                  <div class="service-box text-center">
                     <div class="service-ico mb-3">
                        <img src="images/icon/custody.png" alt="">
                     </div>
                     <h3 class="theme-title"><a href="">Custody</a></h3>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                  <div class="service-box text-center">
                     <div class="service-ico mb-3">
                        <img src="images/icon/defi.png" alt="">
                     </div>
                     <h3 class="theme-title"><a href="">DeFi</a></h3>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                  <div class="service-box text-center">
                     <div class="service-ico mb-3">
                        <img src="images/icon/financing.png" alt="">
                     </div>
                     <h3 class="theme-title"><a href="">Financing</a></h3>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                  <div class="service-box text-center">
                     <div class="service-ico mb-3">
                        <img src="images/icon/brokerage.png" alt="">
                     </div>
                     <h3 class="theme-title"><a href="">Brokerage </a></h3>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="digital-asset border-top page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Flexible Digital Asset Integrations</h2>
                     <p>We include many of the top growing blockchain protocols and token standards and work coordinately with digital asset projects to expand our offer as the ecosystem grows.</p>
                  </div>
               </div>
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="digital-media mt-5">
                     <img src="images/asset-integration.png">
                  </div>
                  <div class="digital-media responsive-media text-center mt-5">
                     <img src="images/asset-integration-responsive.png">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-video page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Ready to learn more?</h2>
                     <p>Jump into DEFINIS’ ecosystem by <br>clicking the button below.</p>
                  </div>
                  <div class="service-btn text-center mt-4">
                     <a class="btn-main get-started" href="https://app.definis.io/register">GET STARTED</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>