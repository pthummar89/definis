<!DOCTYPE html>
<html lang="zxx">
   <head>
      <?php 
         // die($_SERVER['REQUEST_URI']);
         define('BASE_URL', 'https://www.definis.io');
         function url($path){
            return BASE_URL . $path;
         }
         $uri = explode('/',$_SERVER['REQUEST_URI']);
         $page = end($uri);
      ?>
      <title>DEFINIS</title>
      <link rel="icon" href="images/icon.png" type="image/gif" sizes="16x16">
      <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
      <meta content="width=device-width, initial-scale=1.0" name="viewport" />
      <meta content="" name="description" />
      <meta content="" name="keywords" />
      <meta content="" name="author" />
      <!-- CSS Files
         ================================================== -->
      <link href="css/style.css" rel="stylesheet" type="text/css" />
      <link href="css/responsive.css" rel="stylesheet" type="text/css" />
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="css/owl.carousel.min.css" rel="stylesheet" type="text/css" />
      <link href="fontawesome/css/all.min.css" rel="stylesheet" type="text/css" />
   </head>
   <body>
      <header class="header-main">
         <div class="container">
            <div class="row">
               <div class="col-xl-2 col-lg-1 col-md-4 col-sm-12 col-12">
                  <div class="header-logo">
                     <a href="<?php echo url(''); ?>"><span></span></a>
                  </div>
               </div>
               <div class="col-xl-8 col-lg-8 col-md-4 col-sm-12 col-12">
                  <div class="header-nav-menu">
                     <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                           <ul class="navbar-nav mr-auto">
                              <li class="nav-item <?php echo $page=="" ? 'active' : ''; ?>">
                                 <a class="nav-link" href="<?php echo url(''); ?>">Home</a>
                              </li>
                              <li class="nav-item dropdown <?php echo $page=="custody" ? 'active' : ''; ?>">
                                 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-angle-down"></i>
                                 DEFINIS Digital Solutions
                                 </a>
                                 <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item active" href="<?php echo url('/custody')."#stablecoin-payment" ?>">Stablecoin Payment</a>
                                    <a class="dropdown-item" href="<?php echo url('/custody')."#custody" ?>">DEFINIS Custody</a>
                                    <!-- <a class="dropdown-item" href="<?php echo url('/custody')."#staking" ?>">Definis Staking</a> -->
                                    <a class="dropdown-item" href="<?php echo url('/custody')."#otc-trading" ?>">OTC Trading</a>
                                    <!-- <a class="dropdown-item" href="<?php echo url('/custody')."#finacing" ?>"href="$absolute_echo url#">Definis Finacing</a> -->
                                    <a class="dropdown-item" href="<?php echo url('/custody')."#blockchain-audit" ?>">Blockchain Audit</a>
                                 </div>
                              </li>
                              <li class="nav-item <?php echo $page=="financial-institutions" ? 'active' : ''; ?>">
                                 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-angle-down"></i>
                                 Who We Serve
                                 </a>
                                 <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo url('/financial-institutions')."#financial-institutions" ?>">Traditional Finance Institutions TradFi</a>
                                    <a class="dropdown-item" href="<?php echo url('/financial-institutions')."#digital-organizations" ?>">Digital Asset Institutions</a>
                                    <a class="dropdown-item" href="<?php echo url('/financial-institutions')."#corporate-clients" ?>">Family Office and Private Clients</a>
                                 </div>   
                              </li>
                              <li class="nav-item <?php echo $page=="about" ? 'active' : ''; ?>">
                                 <a class="nav-link" href="<?php echo url('/about'); ?>">About Us</a>
                              </li>
                              
                           </ul>
                        </div>
                     </nav>
                  </div>
               </div>
               <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12 col-12">
                  <div class="header-right">
                     <ul class="d-flex align-items-center">
                        <li class="mr-3"><a class="login-text" href="https://app.definis.io/login">Login</a></li>
                        <li><a class="btn-main get-started" href="https://app.definis.io/register">GET STARTED</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </header>