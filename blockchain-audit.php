<?php include('header.php'); ?>
      <section class="hero-service">
         <div class="bg-media">
            <img src="images/slider-dot.png" alt="">
         </div>
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                  <div class="hero-service-info">
                     <div class="page-title text-left">
                        <h4>DEFINIS Blockchain Audit</h4>
                        <h1>The Digital Future of Blockchain Audit</h1>
                        <p>Get into the seamless blockchain audit of DEFINIS.</p>
                     </div>
                     <div class="hero-btn">
                        <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                  <div class="hero-service-media text-center">
                     <!-- <img src="images/future.png" alt=""> -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-grid page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Everything About Blockchain Audit on DEFINIS</h2>
                     <p>A blockchain audit takes place to support high-value transactions on a blockchain. This process contains the use of typical code analysis that recognizes the cons of the system and abolishes any risks in such applications.</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/blockchain-audit/custody-execution-services.png">
                     </div>
                     <h3 class="theme-title">Custody & Execution Services</h3>
                     <p class="theme-description">Digital asset holders on DEFINIS can use a private key to keep their assets secure. Execution services tend to strengthen traders’ global trading capability.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/blockchain-audit/stablecoin-payments.png">
                     </div>
                     <h3 class="theme-title">Stablecoin Payments</h3>
                     <p class="theme-description">DEFINIS supports stablecoin payments to allow traders to pay using stablecoins without worrying about a big price fluctuation of the same.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/blockchain-audit/settlements-and-tri-party-arrangements.png">
                     </div>
                     <h3 class="theme-title">Settlement and tri-party arrangements</h3>
                     <p class="theme-description">Trading activity can be settled from the safety of DEFINIS custody either on behalf of clients or directly by formal deals within merchants or traders.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/blockchain-audit/packaged-offerings.png">
                     </div>
                     <h3 class="theme-title">Packaged offerings</h3>
                     <p class="theme-description">Bundling crypto products and service offerings can help you capitalize on the market. Meeting Clients’ demands may help you gain trust.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/blockchain-audit/legal-regulatory-compliance-planning.png">
                     </div>
                     <h3 class="theme-title">Legal, Regulatory, & Compliance Planning</h3>
                     <p class="theme-description">Analyze legal risks for existing execution paths, implant similarities and reports of crypto products and service solutions.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/blockchain-audit/api-integrations-with-robust-testing.png">
                     </div>
                     <h3 class="theme-title">API Integrations With Robust Testing</h3>
                     <p class="theme-description">DEFINIS can be integrated into relevant platforms with the help of DEFINIS API, which is tested to avoid crashes and errors.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-video page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Ready to learn more?</h2>
                     <p>Jump into DEFINIS’ ecosystem by <br>clicking the button below.</p>
                  </div>
                  <div class="service-btn text-center mt-4">
                     <a class="btn-main get-started" href="https://app.definis.io/register">GET STARTED</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>