<?php include('header.php'); ?>
         <!-- <section class="service-grid page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>The new standard for crypto custody</h2>
                        <p>DEFINIS Custody Infrastructure is specifically designed for the safekeeping of digital assets and connecting with TradFi banking system, all you have to do is to enjoy the insitutional-grade custody.</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/secure.svg">
                        </div>
                        <h3 class="theme-title">Secure</h3>
                        <p class="theme-description">Custody in an advanced digital asset platform to ensure the highest level of security. You can add a private key to access your digital assets on DEFINIS.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/efficient.svg">
                        </div>
                        <h3 class="theme-title">Efficient</h3>
                        <p class="theme-description">The processing time of ongoing transactions on DEFINIS is proven to be faster, within seconds. You can move your funds at any time swiftly and effortlessly.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/customizable.svg">
                        </div>
                        <h3 class="theme-title">Customizable</h3>
                        <p class="theme-description">You can customize the DEFINIS platform to match your priorities and professional needs. Full custidy and settlement APIs for seamless integrations.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/insured.svg">
                        </div>
                        <h3 class="theme-title">Insured</h3>
                        <p class="theme-description">Non-predictable damages to digital assets are protected by insurance, which is provided by DEFINIS.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/auditable.svg">
                        </div>
                        <h3 class="theme-title">Auditable</h3>
                        <p class="theme-description">The existence of accounts of DEFINIS can be proven by their unique private keys assuredly.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/accountable.svg">
                        </div>
                        <h3 class="theme-title">Accountable</h3>
                        <p class="theme-description">Account dashboard or API integration can be used to export structured data. Performance and portfolio status can be tracked at any moment.</p>
                     </div>
                  </div>
               </div>
            </div>
         </section> -->
      </div>
      <div class="main-sec" id="stablecoin-payment">
         <section class="hero-service">
            <div class="bg-media">
               <img src="images/slider-dot.png" alt="">
            </div>
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="hero-service-info">
                        <div class="page-title text-left">
                           <h4>The Digital Future Of Payment</h4>
                           <h1>Stablecoin Payment Gateway for All Kinds Of Needs </h1>
                           <p>Grow your business with a gateway that will save you time and money.</p>
                        </div>
                        <div class="hero-btn">
                           <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="hero-service-media text-center">
                        <!-- <img src="images/future.png" alt=""> -->
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="future-area position-relative page-paddings">
            <div class="future-bg">
               <img src="images/slider-dot-left.png" alt="">
            </div>
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>The Next-Gen Payment on DEFINIS</h2>
                        <p>Users can use DEFINIS for stablecoin payment. DEFINIS supports a whole range of stablecoins owing to its built-in integration with various platforms.</p>
                     </div>
                  </div>
               </div>
               <div class="row justify-content-center future-main">
                  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                     <div class="future-item">
                        <div class="future-ico">
                           <img src="images/icon/next-gen-payment/instant-payouts.svg">   
                        </div>
                        <h3 class="theme-title">Instant Payouts</h3>
                        <p class="theme-description">The real-time crypto-to-fiat conversion by DEFINIS enables it to provide fiat currency payouts to the sellers.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                     <div class="future-item">
                        <div class="future-ico">
                           <img src="images/icon/next-gen-payment/low-fee.svg">   
                        </div>
                        <h3 class="theme-title">Low-fee</h3>
                        <p class="theme-description">Profit from our competitive rate at full transparency amongst top-tier payment solution providers. No volatile rates. No hidden fees.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                     <div class="future-item">
                        <div class="future-ico">
                           <img src="images/icon/next-gen-payment/easy-to-implement.svg">   
                        </div>
                        <h3 class="theme-title">Easy-to-implement</h3>
                        <p class="theme-description">To pay with stablecoins, the user only needs to scan the QR code provided on the webpage of the merchant or from digital wallets.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                     <div class="future-item">
                           <div class="future-ico">
                              <img src="images/icon/next-gen-payment/data-repository.svg">   
                           </div>
                           <h3 class="theme-title">Data Repository</h3>
                           <p class="theme-description">On top of the stablecoin payment gateway, DEFINIS also offers repository services on behalf of the Blockchain technology.</p>
                        </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                     <div class="future-item">
                           <div class="future-ico">
                              <img src="images/icon/next-gen-payment/quick-easy-setup.svg">   
                           </div>
                           <h3 class="theme-title">Quick & Easy SetUp</h3>
                           <p class="theme-description">Merchants and sellers who do not have any prior knowledge of programming can integrate the gateway all by themselves easily.</p>
                        </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                     <div class="future-item">
                           <div class="future-ico">
                              <img src="images/icon/next-gen-payment/error-free.svg">   
                           </div>
                           <h3 class="theme-title">Error-Free</h3>
                           <p class="theme-description">Transactions cannot be canceled or reversed on Blockchain.</p>
                        </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <div class="custody-box" id="custody">
         <section class="hero-service">
            <div class="bg-media">
               <img src="images/slider-dot.png" alt="">
            </div>
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="hero-service-info">
                        <div class="page-title text-left">
                           <h4>The Digital Future of Finance</h4>
                           <h1>Be a Custodian of Your Own Assets on DEFINIS</h1>
                           <p>Safeguard your digital assets in the most secure and compliant way.</p>
                        </div>
                        <div class="hero-btn">
                           <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="hero-service-media text-center">
                        <!-- <img src="images/future.png" alt=""> -->
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="service-grid page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>The new standard for crypto custody</h2>
                        <p>DEFINIS Custody Infrastructure is specifically designed for the safekeeping of digital assets and connecting with TradFi banking system, all you have to do is to enjoy the insitutional-grade custody.</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/secure.svg">
                        </div>
                        <h3 class="theme-title">Secure</h3>
                        <p class="theme-description">Custody in an advanced digital asset platform to ensure the highest level of security. You can add a private key to access your digital assets on DEFINIS.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/efficient.svg">
                        </div>
                        <h3 class="theme-title">Efficient</h3>
                        <p class="theme-description">The processing time of ongoing transactions on DEFINIS is proven to be faster, within seconds. You can move your funds at any time swiftly and effortlessly.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/customizable.svg">
                        </div>
                        <h3 class="theme-title">Customizable</h3>
                        <p class="theme-description">You can customize the DEFINIS platform to match your priorities and professional needs. Full custidy and settlement APIs for seamless integrations.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/insured.svg">
                        </div>
                        <h3 class="theme-title">Insured</h3>
                        <p class="theme-description">Non-predictable damages to digital assets are protected by insurance, which is provided by DEFINIS.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/auditable.svg">
                        </div>
                        <h3 class="theme-title">Auditable</h3>
                        <p class="theme-description">The existence of accounts of DEFINIS can be proven by their unique private keys assuredly.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/custody/accountable.svg">
                        </div>
                        <h3 class="theme-title">Accountable</h3>
                        <p class="theme-description">Account dashboard or API integration can be used to export structured data. Performance and portfolio status can be tracked at any moment.</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <!-- <div class="staking-area" id="staking">
         <section class="hero-service">
            <div class="bg-media">
               <img src="images/slider-dot.png" alt="">
            </div>
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="hero-service-info">
                        <div class="page-title text-left">
                           <h4>The Digital Future of Staking</h4>
                           <h2>Unbelievable Rewards With DEFINIS Staking</h2>
                           <p>Earn passive staking rewards by activating your idle digital assets.</p>
                        </div>
                        <div class="hero-btn">
                           <a href="javascript:void(0)" class="btn-main">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="hero-service-media text-center">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="service-grid page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>DEFINIS Takes Crypto Staking to the Next Level</h2>
                        <p>Staking refers to locking crypto assets for a set of the time period to help the validation of the blockchain. DEFINIS allows the staking of crypto assets straight from custody so that users can grow the portfolio value by utilizing the idle crypto assets with ease.</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/staking/secure-staking.svg">
                        </div>
                        <h3 class="theme-title">Secure Staking</h3>
                        <p class="theme-description">DEFINIS users can securely lock crypto assets for a time period to support the operation of the respective blockchain.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/staking/continuous-new-additions.svg">
                        </div>
                        <h3 class="theme-title">Continuous New Additions</h3>
                        <p class="theme-description">DEFINIS smartly analyzes the market and introduces new proof-of-stake assets that will enhance the experience of staking on DEFINIS.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/staking/flexible-delegation.svg">
                        </div>
                        <h3 class="theme-title">Flexible Delegation</h3>
                        <p class="theme-description">Users can delegate the digital assets on DEFINIS to conduct validation for PoS blockchains, you can either stake through DEFINIS or run your own validator.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/staking/automatic-re-delegation.svg">
                        </div>
                        <h3 class="theme-title">Automatic Re-Delegation</h3>
                        <p class="theme-description">Users can automatically re-delegate the staking rewards on DEFINIS to reduce efforts and time in the staking process.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/staking/self-serve-delegation.svg">
                        </div>
                        <h3 class="theme-title">Self-Serve Delegation</h3>
                        <p class="theme-description">Staking can be delegated by users to another validator in a single delegation request on DEFINIS.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/staking/detailed-reporting.svg">
                        </div>
                        <h3 class="theme-title">Detailed Reporting</h3>
                        <p class="theme-description">DEFINIS provides the detailed reporting of staking rewards to DEFINIS users for review or tax return purpose.</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div> -->
      <div class="otc-trading-area" id="otc-trading">
         <section class="hero-service">
            <div class="bg-media">
               <img src="images/slider-dot.png" alt="">
            </div>
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="hero-service-info">
                        <div class="page-title text-left">
                           <h4>The Digital Future of OTC Trading</h4>
                           <h2>OTC Trading Brings the Freedom</h2>
                           <p>Trade crypto efficiently and smartly, with all the tools you need.</p>
                        </div>
                        <div class="hero-btn">
                           <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="hero-service-media text-center">
                        <!-- <img src="images/future.png" alt=""> -->
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="service-grid page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>The New Heights of OTC Trading</h2>
                        <p>DEFINIS lets traders trade directly with another trader without the supervision of an exchange. OTC trading has the potential of facilitating liquidity, providing transparency, and reducing the price impact.</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/otc-trading/broad-liquidity.svg">
                        </div>
                        <h3 class="theme-title">Broad Liquidity</h3>
                        <p class="theme-description">Access to deep liquidity pools from a single onboarding to achieve the best execution.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/otc-trading/best-execution.svg">
                        </div>
                        <h3 class="theme-title">Best Execution</h3>
                        <p class="theme-description">Execution services strengthen traders’ global trading capability. DEFINIS provides the best prices from a wide range of liquidity pools, exchanges and OTC desks.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/otc-trading/365-access.svg">
                        </div>
                        <h3 class="theme-title">24/7/365 Access</h3>
                        <p class="theme-description">The digital asset platform of DEFINIS is accessible 24x7. The platform is integrated online to make it available anytime, anywhere.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/otc-trading/no-pre-funding.svg">
                        </div>
                        <h3 class="theme-title">No Pre-Funding</h3>
                        <p class="theme-description">Pre-funding of user accounts isn’t necessary at all. Users can have a better capital efficiency using the DEFINIS platform.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/otc-trading/fast-automatic-settlement.svg">
                        </div>
                        <h3 class="theme-title">Fast Automatic Settlement</h3>
                        <p class="theme-description">Settlements on DEFINIS makes the creation and maintenance of control accounts simple, post-trade settlements keeps the record clearly.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/otc-trading/transparent-pricing.svg">
                        </div>
                        <h3 class="theme-title">Transparent Pricing</h3>
                        <p class="theme-description">The pricing of digital assets on DEFINIS doesn’t contain any hidden charges. DEFINIS maintains transparency in terms of pricing.</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <!-- <div class="finacing-area" id="finacing">
         <section class="hero-service">
            <div class="bg-media">
               <img src="images/slider-dot.png" alt="">
            </div>
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="hero-service-info">
                        <div class="page-title text-left">
                           <h4>The Digital Future Of Financing</h4>
                           <h2>Show Your Dominance by Financing</h2>
                           <p>Access yield opportunities for digital asset lenders and liquidity for borrowers.</p>
                        </div>
                        <div class="hero-btn">
                           <a href="javascript:void(0)" class="btn-main">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="hero-service-media text-center">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="service-grid page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>The Next-Gen Financing on DEFINIS</h2>
                        <p>Investors can use DEFINIS for digital assets financing. By Financing, investors have the chance to earn passive income in terms of digital assets such as crypto.</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/finance/straightforward-processes.svg">
                        </div>
                        <h3 class="theme-title">Straightforward Processes</h3>
                        <p class="theme-description">Transaction processes and other respective processes are transparent and straightforward on DEFINIS to avoid inconsistencies.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/finance/compliant-crypto-financing.svg">
                        </div>
                        <h3 class="theme-title">Compliant Crypto Financing</h3>
                        <p class="theme-description">Crypto financing on DEFINIS follows AML cryptocurrency regulations to achieve maximum compliance, with the aim to prevent frauds.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/finance/stablecoin-lending-and-borrowing.svg">
                        </div>
                        <h3 class="theme-title">Stablecoin Lending And Borrowing</h3>
                        <p class="theme-description">Lending and borrowing can take place between users on DEFINIS platform and more sources, so that the needs of both qualified borrowers and lenders are satisfied.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/finance/strategic-loan-structuring.svg">
                        </div>
                        <h3 class="theme-title">Strategic Loan Structuring</h3>
                        <p class="theme-description">Loan structure can be constructed by various factors, including the condition of borrowing requests and the client's risk factor.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/finance/integrated-services.svg">
                        </div>
                        <h3 class="theme-title">Integrated Services</h3>
                        <p class="theme-description">DEFINIS has integrated various services related to digital assets like custody, stablecoin payments, staking, trading, and settlements, etc.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/finance/broad-and-expanding-asset-support.svg">
                        </div>
                        <h3 class="theme-title">Broad And Expanding Asset Support</h3>
                        <p class="theme-description">DEFINIS supports a wide range of crypto assets financing, from all the major stablecoins to BTC and ETH, etc.</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div> -->
      <div class="blockchain-audit-area" id="blockchain-audit">
         <section class="hero-service">
            <div class="bg-media">
               <img src="images/slider-dot.png" alt="">
            </div>
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="hero-service-info">
                        <div class="page-title text-left">
                           <h4>The Digital Future of Blockchain Audit</h4>
                           <h2>Flawless High-Value Transactions On DEFINIS</h2>
                           <p>Experiencing Smooth high-value transactions on DEFINIS by leveraging Blockchain Audit</p>
                        </div>
                        <div class="hero-btn">
                           <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="hero-service-media text-center">
                        <!-- <img src="images/future.png" alt=""> -->
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="service-grid page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>The New Standard For Blockchain Audit</h2>
                        <p>A blockchain audit takes place to support high-value transactions on a blockchain. This process contains the use of typical code analysis that recognizes cons in the system and abolishes any risks in such applications.</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/blockchain-audit/custody-execution-services.svg">
                        </div>
                        <h3 class="theme-title">Custody & Execution Services</h3>
                        <p class="theme-description">Digital asset holders on DEFINIS can use a private key to keep their assets secure. Execution services tend to strengthen traders’ global trading capability.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/blockchain-audit/stablecoin-payments.svg">
                        </div>
                        <h3 class="theme-title">Stablecoin Payments</h3>
                        <p class="theme-description">DEFINIS supports stablecoin payments to allow traders to pay using stablecoins without worrying about a big price fluctuation of the same.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/blockchain-audit/settlements-and-tri-party-arrangements.svg">
                        </div>
                        <h3 class="theme-title">Settlement And Tri-Party Arrangements</h3>
                        <p class="theme-description">Trading activity can be settled from the safety of DEFINIS custody either on behalf of clients or directly by formal deals within merchants or traders.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/blockchain-audit/packaged-offerings.svg">
                        </div>
                        <h3 class="theme-title">Packaged Offerings</h3>
                        <p class="theme-description">Bundling crypto products and service offerings can help you capitalize on the market. Meeting Clients’ demands may help you gain trust.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/blockchain-audit/legal-regulatory-compliance-planning.svg">
                        </div>
                        <h3 class="theme-title">Legal, Regulatory, & Compliance Planning</h3>
                        <p class="theme-description">Analyze legal risks for existing execution paths, implant similarities and reports of crypto products and service solutions.</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                     <div class="service-item">
                        <div class="service-ico">
                           <img src="images/icon/blockchain-audit/api-integrations-with-robust-testing.svg">
                        </div>
                        <h3 class="theme-title">API Integrations With Robust Testing</h3>
                        <p class="theme-description">DEFINIS can be integrated into relevant platforms with the help of DEFINIS API, which is tested to avoid crashes and errors.</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <section class="service-video page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Ready to learn more?</h2>
                     <p>Jump into DEFINIS ecosystem by <br>clicking the button below.</p>
                  </div>
                  <div class="service-btn text-center mt-4">
                     <a class="btn-main get-started" href="https://app.definis.io/register">GET STARTED</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>