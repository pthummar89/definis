<?php include('header.php'); ?>
      <section class="about-area page-paddings">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                  <div class="about-box">
                     <div class="page-title text-left">
                        <h2>About Us</h2>
                        <p class="small-text-about">DEFINIS acts as a bridge between traditional finance and the digital & crypto financial market by providing a seamless digital financial experience to investors with an accessible approach in crypto markets. Our services include real-time stablecoin-fiat swap, OTC trading, custody, investment and a lot more to able to accommodate both corporate and individual customers. Our company partners with established proprietary networks and industry leaders facilitate us to grasp valuable opportunities from fast-paced world of web3.</p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                  <div class="about-media text-center">
                     <!-- <img src="images/future.png" alt=""> -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="leadership-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Our Team</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">
                  <div class="leadership-item text-center">
                     <div class="leadership-media">
                        <img src="images/team/team1.jpg" alt="">
                     </div>
                     <h3 class="theme-title">Kurt Li</h3>
                     <p class="theme-description">Founder</p>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">
                  <div class="leadership-item text-center">
                     <div class="leadership-media">
                        <img src="images/team/team2.jpg" alt="">
                     </div>
                     <h3 class="theme-title">Robin Ho</h3>
                     <p class="theme-description">CEO, Co-Founder</p>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">
                  <div class="leadership-item text-center">
                     <div class="leadership-media">
                        <img src="images/team/team3.jpg" alt="">
                     </div>
                     <h3 class="theme-title">Nicole Camille</h3>
                     <p class="theme-description">Chief Marketing Officer</p>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">
                  <div class="leadership-item text-center">
                     <div class="leadership-media">
                        <img src="images/team/team4.jpg" alt="">
                     </div>
                     <h3 class="theme-title">Simon</h3>
                     <p class="theme-description">Chief Financial Officer</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-video page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Ready to learn more?</h2>
                     <p>Jump into DEFINIS’ ecosystem by <br>clicking the button below.</p>
                  </div>
                  <div class="service-btn text-center mt-4">
                     <a class="btn-main get-started" href="https://app.definis.io/register" >GET STARTED</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>