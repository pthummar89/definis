$(window).scroll(function() {
    if ($(window).scrollTop() >= 300) {
        $('.header-main').addClass('fixed-header');
    } else {
        $('.header-main').removeClass('fixed-header');
    }
});


$(document).ready(function() {
    url = window.location.href;
    $(".dropdown-menu .dropdown-item").each(function(index, elem) {
        var $elem = $(elem);
        $elem.removeClass('active');
        if ($elem.prop('href') == url) {
            $elem.addClass('active');
        }
    });
    
    // $('.dropdown-item').click(function(){
    //     hash = $(this).attr('href');
    //     hash = hash.split('#').pop();
    //     // $('html,body').animate({ scrollTop: $('#'+hash).offset().top - 100 }, 100);
    // })
    $(document).on('click','.dropdown-menu .dropdown-item',function(){
        target = $(this).attr('href');
        var target = this.hash;
        var $target = $(target);
        console.log(target,$target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function() {
            window.location.hash = target;
        });
        $(this).addClass('active').siblings().removeClass('active');
    })
    // $('.dropdown-menu .dropdown-item[href^="#"]').on('click', function(e) {
    //     debugger
    //     target = $(this).attr('href');
    //     var target = this.hash;
    //     var $target = $(target);
    //     console.log(target,$target);
    //     $('html, body').stop().animate({
    //         'scrollTop': $target.offset().top
    //     }, 900, 'swing', function() {
    //         window.location.hash = target;
    //     });
    //     $(this).addClass('active').siblings().removeClass('active');
    // });
    // $('html,body').animate({ scrollTop: $(hash).offset().top - 100 }, 'fast');

});

$("#login-tab, #footer-login-tab").click(function() {
    $("#login-div").addClass("show");
    $("#login-box").removeClass("d-none");
    $("#forgot-pass-box").addClass("d-none");
    $("#signup-box").addClass("d-none");
});

$(".login-close").click(function() {
    $(".login-main.login-popup").removeClass("show");
    $("#login-box").addClass("d-none");
    $("#forgot-pass-box").addClass("d-none");
    $("#signup-box").addClass("d-none");
});

$(".signup-tab").click(function() {
    $("#login-div").addClass("show");
    $("#signup-box").removeClass("d-none");
    $("#login-box").addClass("d-none");
    $("#forgot-pass-box").addClass("d-none");
});


$(".signup-link").click(function() {
    $("#login-box").addClass("d-none");
    $("#forgot-pass-box").addClass("d-none");
    $("#signup-box").removeClass("d-none");
});

$(".login-link").click(function() {
    $("#signup-box").addClass("d-none");
    $("#login-box").removeClass("d-none");
    $("#forgot-pass-box").addClass("d-none");
});

$(".forgot-pass-link").click(function() {
    $("#signup-box").addClass("d-none");
    $("#login-box").addClass("d-none");
    $("#forgot-pass-box").removeClass("d-none");
});

// $("#signup-link").click(function(){
//   $("#login-popup").addClass("show");
// });


var btn = $('#arrow-top');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});

