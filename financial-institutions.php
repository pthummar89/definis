<?php include('header.php'); ?>
         <section class="hero-service financial-section" id="financial-institutions">
            <div class="bg-media">
               <img src="images/slider-dot.png" alt="">
            </div>
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="hero-service-info">
                        <div class="page-title text-left">
                           <h4>For Financial Institutions TradFi</h4>
                           <h1>We Promote Mass Institutional Adoption</h1>
                           <p>DEFINIS has all the features needed to assist financial institutions achieving the goals.</p>
                        </div>
                        <div class="hero-btn">
                           <a href="javascript:void(0)" class="btn-main">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="hero-service-media text-center">
                        <!-- <img src="images/future.png" alt=""> -->
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="who-we-area page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>Our Institutional Partners</h2>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="finacing-media text-center mt-4">
                        <img src="images/financial-institutions.svg" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="finance-area page-background">
            <div class="container">
               <div class="row">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="finance-media">
                        <img src="images/screenshot-2.png" alt="">
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="finance-info">
                        <div class="page-title text-left">
                           <h2>A Platform Built For Financial Institutions</h2>
                        </div>
                        <ul>
                           <li>Auditability</li>
                           <li>Efficiency</li>
                           <li>Transparency</li>
                           <li>Sub-Accounts</li>
                           <li>Bank-Grade Workflows</li>
                           <li>Multi-Wallet Infrastructure</li>
                           <li>Transaction Policies</li>
                           <li>Integrated Compliance</li>
                           <li>Connectivity Interfaces</li>
                           <li>Multi-Layer Security</li>
                        </ul>
                        <div class="finance-btn">
                           <!-- <a href="index.php" class="btn-main">See more</a> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="service-area page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>Compliance-Focused Services</h2>
                        <p>We offer full-fledged services to integrate new digital assets as they grow and adapt to the growing market, all while meeting compliance standards within a developing regulatory framework. We are here to add value to you, and grow together.</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                     <div class="service-box text-center">
                        <div class="service-ico mb-3">
                           <img src="images/icon/custody.svg" alt="">
                        </div>
                        <h3 class="theme-title"><a href="">Custody</a></h3>
                     </div>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                     <div class="service-box text-center">
                        <div class="service-ico mb-3">
                           <img src="images/icon/defi.svg" alt="">
                        </div>
                        <h3 class="theme-title"><a href="">DeFi</a></h3>
                     </div>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                     <div class="service-box text-center">
                        <div class="service-ico mb-3">
                           <img src="images/icon/financing.svg" alt="">
                        </div>
                        <h3 class="theme-title"><a href="">Financing</a></h3>
                     </div>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                     <div class="service-box text-center">
                        <div class="service-ico mb-3">
                           <img src="images/icon/brokerage.svg" alt="">
                        </div>
                        <h3 class="theme-title"><a href="">Brokerage </a></h3>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <div class="digital-organizations-area" id="digital-organizations">
         <section class="hero-service financial-section">
            <div class="bg-media">
               <img src="images/slider-dot.png" alt="">
            </div>
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="hero-service-info">
                        <div class="page-title text-left">
                           <h4>For Digital Asset Organizations</h4>
                           <h1>Safeguard Your Digital Asset Business</h1>
                           <p>Digital asset organizations can secure their digital asset businesses with the help of the features like custody-execution services on DEFINIS.</p>
                        </div>
                        <div class="hero-btn">
                           <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="hero-service-media text-center">
                        <!-- <img src="images/future.png" alt=""> -->
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="who-we-area page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>Solutions for Web3 Organizations</h2>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="finacing-media text-center mt-4">
                        <img src="images/who-we-serve.svg" alt="">
                     </div>
                     <div class="finacing-media responsive-media text-center mt-4">
                        <img src="images/who-we-serve-responsive.svg" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="finance-area page-background">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                     <div class="finance-media">
                        <img src="images/screenshots1.png" alt="">
                     </div>
                  </div>
                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                     <div class="finance-info">
                        <div class="page-title text-left">
                           <h2>A Bank-Grade Platform For Managing Digital Assets</h2>
                        </div>
                        <ul>
                           <li>Auditability</li>
                           <li>Efficiency</li>
                           <li>Transparency</li>
                           <li>Sub-Accounts</li>
                           <li>Bank-Grade Workflows</li>
                           <li>Multi-Wallet Infrastructure</li>
                           <li>Transaction Policies</li>
                           <li>Integrated Compliance</li>
                           <li>Connectivity Interfaces</li>
                           <li>Multi-Layer Security</li>
                        </ul>
                        <div class="finance-btn">
                           <!-- <a href="index.php" class="btn-main">See more</a> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="service-area page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>Digital Asset Services</h2>
                        <p>DEFINIS offers full-fledged services connected to the wider digital asset ecosystem. From custody and staking to trading, DEFINIS offers a wide range of digital asset services and solutions supported by dedicated customer service available 24x7.</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                     <div class="service-box text-center">
                        <div class="service-ico mb-3">
                           <img src="images/icon/custody.svg" alt="">
                        </div>
                        <h3 class="theme-title"><a href="">Custody</a></h3>
                     </div>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                     <div class="service-box text-center">
                        <div class="service-ico mb-3">
                           <img src="images/icon/defi.svg" alt="">
                        </div>
                        <h3 class="theme-title"><a href="">DeFi</a></h3>
                     </div>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                     <div class="service-box text-center">
                        <div class="service-ico mb-3">
                           <img src="images/icon/financing.svg" alt="">
                        </div>
                        <h3 class="theme-title"><a href="">Financing</a></h3>
                     </div>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                     <div class="service-box text-center">
                        <div class="service-ico mb-3">
                           <img src="images/icon/brokerage.svg" alt="">
                        </div>
                        <h3 class="theme-title"><a href="">Brokerage </a></h3>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="digital-asset border-top page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>Flexible Digital Asset Integrations</h2>
                        <p>We include many of the popular and top growing blockchain protocols and token standards and work coordinately with digital asset project teams to expand our offer as the ecosystem grows.</p>
                     </div>
                  </div>
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="digital-media mt-5">
                        <img src="images/asset-integration.png">
                     </div>
                     <div class="digital-media responsive-media text-center mt-5">
                        <img src="images/asset-integration-responsive.png">
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <div class="corporate-clients-area" id="corporate-clients">
         <section class="hero-service financial-section">
            <div class="bg-media">
               <img src="images/slider-dot.png" alt="">
            </div>
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                     <div class="hero-service-info">
                        <div class="page-title text-left">
                           <h4>For Corporate & Private Clients</h4>
                           <h1>Embrace The Future of Finance</h1>
                           <p>DEFINIS has a set of extraordinary features to fulfil the needs of corporate & private clients.</p>
                        </div>
                        <div class="hero-btn">
                           <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                     <div class="hero-service-media text-center">
                        <!-- <img src="images/future.png" alt=""> -->
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="who-we-area page-paddings">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>Solutions for Web3 Organizations</h2>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="finacing-media text-center mt-4">
                        <img src="images/diagram_outlined.svg" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="digital-asset-area page-background">
            <div class="container">
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="page-title">
                        <h2>Manage All Your Digital Assets With DEFINIS</h2>
                        <p>DEFINIS institutional-grade infrastructure has been developed to manage digital assets. We provide hassle-free management of all the popular digital assets with multi-layer security on DEFINIS.</p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                     <div class="digital-assets-box">
                        <div class="digital-media">
                           <img src="images/icon/security-first.svg" alt="">
                        </div>
                        <div class="digital-info">
                           <h3 class="theme-title">Multi-Layer Security</h3>
                           <p class="theme-description">A multi-layer security platform protects all the assets from cyber-attacks, internal collusion and human errors.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                     <div class="digital-assets-box">
                        <div class="digital-media">
                           <img src="images/icon/multi-user-access.svg" alt="">
                        </div>
                        <div class="digital-info">
                           <h3 class="theme-title">Multi-User Access</h3>
                           <p class="theme-description">Users allowed by the account owner can access a single DEFINIS account on multiple devices.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                     <div class="digital-assets-box">
                        <div class="digital-media">
                           <img src="images/icon/comprehensive-insurance.svg" alt="">
                        </div>
                        <div class="digital-info">
                           <h3 class="theme-title">Comprehensive Insurance</h3>
                           <p class="theme-description">Damage of digital assets are protected by the comprehensive insurance carried by DEFINIS.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                     <div class="digital-assets-box">
                        <div class="digital-media">
                           <img src="images/icon/manage-nfts.svg" alt="">
                        </div>
                        <div class="digital-info">
                           <h3 class="theme-title">Manage NFTs</h3>
                           <p class="theme-description">NFTs can be managed securely on DEFINIS along with other digital assets.</p>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="digital-btn mt-5 text-center">
                        <button class="btn-main">See more</button>
                     </div>
                  </div>
               </div> -->
            </div>
         </section>
         <section class="solution-area page-paddings">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                     <div class="solution-info">
                        <div class="page-title text-left">
                           <h2>Solutions Tailored To Your Requirements</h2>
                        </div>
                        <div class="solution-main">
                           <div class="solution-item">
                              <p class="theme-description">Digital assets are protected by MPC Technology and holders on DEFINIS can use a private key to access their assets in the DEFINIS account.</p>
                              <a href="">Custody</a>
                           </div>
                           <div class="solution-item mt-5">
                              <p class="theme-description">The industry-leading DeFi protocols and platforms can be safely handled in your DEFINIS account.</p>
                              <a href="">Defi</a>
                           </div>
                           <div class="solution-item mt-5">
                              <p class="theme-description">Utilize our trading strategies, cutting-edge technology and direct settlement in your custody account.</p>
                              <a href="">OTC Trading</a>
                           </div>
                           <div class="solution-item mt-5">
                              <p class="theme-description">Earn passive returns easily with our liquidity and yield income solutions, specifically made to fulfil your needs.</p>
                              <a href="">Financing & Structured Solutions</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                     <div class="solution-media">
                        <img src="images/screenshot-3.png">
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <section class="service-video page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Ready to learn more?</h2>
                     <p>Jump into DEFINIS ecosystem by <br>clicking the button below.</p>
                  </div>
                  <div class="service-btn text-center mt-4">
                  <a class="btn-main get-started" href="javascript:void(0)">GET STARTED</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>