<?php include('header.php'); ?>
      <section class="slider-area">
         <div class="container">
            <div class="row d-flex align-items-center">
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="slider-main">
                     <h1>DEFINIS</h1>
                     <h2><b>One-stop Digital Asset Solution for Institutions</b></h2>
                     <h4 style="line-height: 1.4;">DEFINIS is the bridge connecting Web 3.0 and TradFi, from custody to staking, OTC trading, financing, and blockchain auditing. Let's shape the digital future of finance with DEFINIS. </h4>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="slider-media">
                     <img src="images/slider-logo.svg" alt="">
                  </div>
               </div>
            </div>
         </div>
         <div class="bg-media">
            <img src="images/slider-dot.png" alt="">
         </div>
      </section>
      <section class="future-area position-relative page-paddings">
         <div class="future-bg">
            <img src="images/slider-dot-left.png" alt="">
         </div>
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>The Infrastructure of Future Digital Finance</h2>
                     <p>"DEFINIS" stems from our simple yet powerful word - 'definite'. As one of the most trusted partners, DEFINIS empowers institutions to capture the enormous growth opportunities in the digital asset space, leveraging our highly secure and compliant bank-grade platform.</p>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center future-main">
               <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="future-item">
                     <div class="future-ico">
                        <img src="images/icon/stability.svg">   
                     </div>
                     <h3 class="theme-title">Stability</h3>
                     <p class="theme-description">State-of-art architecture directly connects stablecoin issuers platforms with DEFINIS, 1:1 USD stablecoin to US Dollar guaranteed without concerning about the volume.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="future-item">
                     <div class="future-ico">
                        <img src="images/icon/transparency.svg">   
                     </div>
                     <h3 class="theme-title">Transparency</h3>
                     <p class="theme-description">We take pride in zero hidden charge for the listed pricing of digital assets on DEFINIS. Our standard is to maintain transparency in all pricings.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="future-item">
                     <div class="future-ico">
                        <img src="images/icon/efficiency.svg">   
                     </div>
                     <h3 class="theme-title">Efficiency</h3>
                     <p class="theme-description">The processing time of ongoing transactions on DEFINIS is proven to be faster, within seconds. You can move your funds at any time swiftly and effortlessly.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="future-item">
                        <div class="future-ico">
                           <img src="images/icon/auditability.svg">   
                        </div>
                        <h3 class="theme-title">Auditability</h3>
                        <p class="theme-description">Each transaction is not only recorded and documented, it is also being audited by licensed auditor specialising in AML. It also allows users to trace all the transactions and trades when needed.</p>
                     </div>
               </div>
               <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="future-item">
                        <div class="future-ico">
                           <img src="images/icon/security.svg">   
                        </div>
                        <h3 class="theme-title">Security</h3>
                        <p class="theme-description">DEFINIS leverages multi-party computation (MPC) technology to safely store and transfer digital assets. A multi-layer security platform protects all the assets from cyber-attacks, internal collusion and human errors.</p>
                     </div>
               </div>
            </div>
         </div>
      </section>
      <!-- <section class="token-area page-background">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>A seamless process for managing <br>your fiat and digital assets.</h2>
                     <p><b>"DEFINIS"</b> is birth through our simple, yet powerful message- 'definite'</p>
                     <p>We guarentee our customers to definitely swap from stable coin to fiat and vice versa safely and speedily.</p>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <div class="token-right">
                     <div class="token-item d-flex align-items-center">
                        <div class="token-ico">
                           <span><img src="images/icon/settle-real.png"></span>
                        </div>
                        <div class="token-detail">
                           <h3 class="theme-title">Settle within seconds</h3>
                           <p class="theme-description">Transaction processes on DEFINIS are settled as soon as possible which means there is almost no waiting period in DEFINIS</p>
                        </div>
                     </div>
                     <div class="token-item d-flex align-items-center">
                        <div class="token-ico">
                           <span><img src="images/icon/traceable-auditable.png"></span>
                        </div>
                        <div class="token-detail">
                           <h3 class="theme-title">Traceable and auditable</h3>
                           <p class="theme-description">Auditability of DEFINIS lets users use the traceability to ensure the needs have been met & to analyze devitations in delivered products.</p>
                        </div>
                     </div>
                     <div class="token-item d-flex align-items-center">
                        <div class="token-ico">
                           <span><img src="images/icon/global-finance.png"></span>
                        </div>
                        <div class="token-detail">
                           <h3 class="theme-title">Utilize 24x7 global finance market</h3>
                           <p class="theme-description">Users of DEFINIS can utilize the global finance market 24x7, DEFINIS’ flexibility lets users explore the global finance market according to their comfort.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <!-- <section class="our-product-area position-relative border-bottom page-paddings">
         <div class="page-bg">
            <img src="images/slider-dot.png" alt="">
         </div>
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h4>Our Product</h4>
                     <h2>Available around the globe and <br>always online</h2>
                     <p>DEFINIS hands you back the control over your assets that you should have never lost. Be the custodian of your own currencies, manage your own funds while eliminating risks of third-party exposure</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="our-product-main">
            <div class="container">
               <div class="row">
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                     <div class="our-product-box border-right d-flex align-items-center">
                        <div class="our-product-media">
                           <img src="images/hkdd.png" alt="">
                        </div>
                        <div class="our-product-info">
                           <h2>HKDD</h2>
                           <p class="theme-description">1:1 pegged to the Hong Kong Dollar</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                     <div class="our-product-box d-flex justify-content-end align-items-center">
                        <div class="our-product-info">
                           <h2>HKDD</h2>
                           <p class="theme-description">1:1 pegged to the Hong Kong Dollar</p>
                        </div>
                        <div class="our-product-media left-margin">
                           <img src="images/hkdd.png" alt="">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <section class="digital-assets-area page-paddings page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>A Single Platform to Manage all your Digital Assets</h2>
                     <p>All the popular digital assets can be managed on DEFINIS expertly. Users can save the hassle from switching platform to platform.</p>
                  </div>
               </div>
            </div>
            <div class="row future-main">
               <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="digital-assets-item">
                     <div class="digital-assets-ico">
                        <span class="digital-media"></span>
                     </div>
                     <h3 class="theme-title">Institutional-Grade Technology</h3>
                     <p class="theme-description">Our pioneering technology is meticulously developed by our leading tech engineers to provide the highest standard of user experience, with security being our top priority.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="digital-assets-item">
                     <div class="digital-assets-ico">
                        <span class="digital-media digital-media-one"></span>
                     </div>
                     <h3 class="theme-title">Regulation-ready</h3>
                     <p class="theme-description">DEFINIS is a licensed institution using comprehensive analytics tools to screen all the transactions for compliance with applicable Anti-Money Laundering (AML), Countering the Financing of Terrorism (CFT) and Sanctions Laws.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="digital-assets-item">
                     <div class="digital-assets-ico">
                        <span class="digital-media digital-media-two"></span>
                     </div>
                     <h3 class="theme-title">Innovative Strategies</h3>
                     <p class="theme-description">DEFINIS adopts innovative strategies in the digital finance space and supports a variety of digital assets, allowing our users to maximize the growth potential with different levels of risk appetite.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- <section class="faq-area page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Customers frequently ask</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="faq-box">
                     <div class="accordion" id="faqone">
                        <div class="card">
                           <div class="card-header" id="faqhead1">
                              <a href="#" class="btn btn-header-link" data-toggle="collapse" data-target="#faq1"
                                 aria-expanded="true" aria-controls="faq1">What are digital assets?</a>
                           </div>
                           <div id="faq1" class="collapse" aria-labelledby="faqhead1" data-parent="#faqone">
                              <div class="card-body">
                                 Digital assets like Bitcoin, Ethereum and Litecoin are cryptographically secured, protocol-governed assets that serve as stores of value, medium of exchange, or merely a decentralized software solution. Digital assets have gained high popularity over the past few years. In fact, digital assets are now integrating with the broader economy and growing into an asset class appropriate to a wide array of investors.
                              </div>
                           </div>
                        </div>
                        <div class="card">
                           <div class="card-header" id="faqhead2">
                              <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq2"
                                 aria-expanded="true" aria-controls="faq2">How does SaaSworld make money?</a>
                           </div>
                           <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faqone">
                              <div class="card-body">
                                 Many SaaS services are free of cost or contain free variants, while improved SaaS is provided at reasonable fees and has a large number of subscribers. This is how SaaS enterprises can have a good number of subscribers who can pay for digital assets. This is the most reliable way of making money with SaaSworld and SaaSworld claims some part of the subscription amount as their revenue.
                              </div>
                           </div>
                        </div>
                        <div class="card">
                           <div class="card-header" id="faqhead3">
                              <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq3"
                                 aria-expanded="true" aria-controls="faq3">Can I sell scripts, etc written by others?</a>
                           </div>
                           <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faqone">
                              <div class="card-body">
                                 Users having an account on DEFINIS can sell available digital assets via DEFINIS. You can go with other creators’ assets as well, but we refer to taking the owner’s permission non-verbally to avoid conflicts such as copyright issues.
                              </div>
                           </div>
                        </div>
                        <div class="card">
                           <div class="card-header" id="faqhead4">
                              <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq4"
                                 aria-expanded="true" aria-controls="faq4">What is a digital asset exchange?</a>
                           </div>
                           <div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faqone">
                              <div class="card-body">
                                 A digital asset exchange is an e-platform that supervises the trading of digital assets. DAX platforms let investors trade allowed digital assets such as Bitcoin (BTC), Ether (ETH), Ripple (XRP), Litecoin (LTC) and Bitcoin Cash (BCH).
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="faq-box">
                     <div class="accordion" id="faq">
                        <div class="card">
                           <div class="card-header" id="faqhead5">
                              <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq5"
                                 aria-expanded="true" aria-controls="faq5">Who can be a DAX operator?</a>
                           </div>
                           <div id="faq5" class="collapse" aria-labelledby="faqhead5" data-parent="#faq">
                              <div class="card-body">
                                 A DAX operator needs to register himself as a Recognized Market Operator as set on the Guidelines on Recognized Markets. The applicant must be able to determine that he is able to fulfil all relevant requirements under the Guidelines on Recognized Markets.
                              </div>
                           </div>
                        </div>
                        <div class="card">
                           <div class="card-header" id="faqhead6">
                              <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq6"
                                 aria-expanded="true" aria-controls="faq6">What are the risks involved with Cryptocurrencies?</a>
                           </div>
                           <div id="faq6" class="collapse" aria-labelledby="faqhead6" data-parent="#faq">
                              <div class="card-body">
                                 Like all investment platforms, Cryptocurrencies also offer risk. Here is a list of the most common risks associated with this alternative investment class: business risks, cyber/theft risks, market risks, regulatory compliance risks etc.
                              </div>
                           </div>
                        </div>
                        <div class="card">
                           <div class="card-header" id="faqhead7">
                              <a href="#" class="btn btn-header-link" data-toggle="collapse" data-target="#faq7"
                                 aria-expanded="true" aria-controls="faq7">What should investors do?</a>
                           </div>
                           <div id="faq7" class="collapse show" aria-labelledby="faqhead7" data-parent="#faq">
                              <div class="card-body">
                                 Before anybody makes an active decision, investors need to check their trading setup. Does your infrastructure support the new hard fork? Are custodians and trading opponents listing it or prepared? As seen with the Terra Luna Airdrop, not every investor can take part in the beginning, because critical service providers are either incapable or unwilling to provide the necessary services.
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="faq-btn text-center">
                     <a class="btn-main" href="">Read All FAQ</a>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <!-- <section class="digital-currencies-area page-paddings">
         <div class="digital-bg">
            <img src="images/questions.png" alt="">
         </div>
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="digital-currencies-box">
                     <div class="page-title">
                        <h2>Why use digital currencies?</h2>
                        <p>Stablecoins are ideal safe haven assets due to the stability of its value, unlike other cryptocurrencies that experience dramatic price fluctuation on the daily. There is no risk of loss for individuals using stablecoins to store value, due to the asset’s stability and self-custodial nature. Stablecoins are also a prime solution for exchanges and institutional traders who want the ability to reduce risk exposure without fully cashing out</p>
                     </div>
                     <div class="currencies__main clearfix">
                        <div class="currencies-list">
                           <div class="currencies-item">
                              <span><img src="images/security.png" alt=""></span>
                           </div>
                           <h3 class="theme-title">Security</h3>
                        </div>
                        <div class="currencies-list">
                           <div class="currencies-item">
                              <span><img src="images/speed.png" alt=""></span>
                           </div>
                           <h3 class="theme-title">Speed</h3>
                        </div>
                        <div class="currencies-list">
                           <div class="currencies-item">
                              <span><img src="images/transparency.png" alt=""></span>
                           </div>
                           <h3 class="theme-title">Transparency</h3>
                        </div>
                        <div class="currencies-list">
                           <div class="currencies-item">
                              <span><img src="images/opportunities.png" alt=""></span>
                           </div>
                           <h3 class="theme-title">Opportunities</h3>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <!-- <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <?php include('footer.php'); ?>
      