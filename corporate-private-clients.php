<?php include('header.php'); ?>
      <section class="hero-service financial-section">
         <div class="bg-media">
            <img src="images/slider-dot.png" alt="">
         </div>
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                  <div class="hero-service-info">
                     <div class="page-title text-left">
                        <h4>For Corporate & Private Clients</h4>
                        <h1>Join The Future Of Investing</h1>
                        <p>DEFINIS has a set of extraordinary features to fulfil the requirements of corporate & private clients.</p>
                     </div>
                     <div class="hero-btn">
                        <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                  <div class="hero-service-media text-center">
                     <!-- <img src="images/future.png" alt=""> -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="who-we-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Potential Service Acceptors</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="finacing-media text-center mt-4">
                     <img src="images/diagram_outlined.svg" alt="">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="digital-asset-area page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Manage All Your Digital Assets With DEFINIS</h2>
                     <p>DEFINIS has been made to manage digital assets and we provide hassle-free management of all the popular digital assets with defense-grade security on DEFINIS.</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <div class="digital-assets-box">
                     <div class="digital-media">
                        <img src="images/icon/security-first.png" alt="">
                     </div>
                     <div class="digital-info">
                        <h3 class="theme-title">Security First</h3>
                        <p class="theme-description">DEFINIS has a defined security level to keep users safe from security threats.</p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <div class="digital-assets-box">
                     <div class="digital-media">
                        <img src="images/icon/multi-user-access.png" alt="">
                     </div>
                     <div class="digital-info">
                        <h3 class="theme-title">Multi-user Access</h3>
                        <p class="theme-description">Users allowed by the account owner can access a single DEFINIS account on multiple devices.</p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <div class="digital-assets-box">
                     <div class="digital-media">
                        <img src="images/icon/comprehensive-insurance.png" alt="">
                     </div>
                     <div class="digital-info">
                        <h3 class="theme-title">Comprehensive Insurance</h3>
                        <p class="theme-description">Damage of digital assets can be protected by the comprehensive insurance of DEFINIS.</p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <div class="digital-assets-box">
                     <div class="digital-media">
                        <img src="images/icon/manage-nfts.png" alt="">
                     </div>
                     <div class="digital-info">
                        <h3 class="theme-title">Manage NFTs</h3>
                        <p class="theme-description">NFTs can be managed securely on DEFINIS along with other digital assets.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="digital-btn mt-5 text-center">
                     <button class="btn-main">See more</button>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="solution-area page-paddings">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-5 col-lg-5 col-md-5 col-sm-6 col-12">
                  <div class="solution-info">
                     <div class="page-title text-left">
                        <h2>Solutions Tailored To Your Requirements</h2>
                     </div>
                     <div class="solution-main">
                        <div class="solution-item">
                           <p class="theme-description">Digital asset holders on DEFINIS can use a private key to keep their assets secure to increase user security on the DEFINIS account.</p>
                           <a href="">Custody</a>
                        </div>
                        <div class="solution-item">
                           <p class="theme-description">The industry-leading DeFi protocols and platforms can be safely handled in your DEFINIS account.</p>
                           <a href="">Defi</a>
                        </div>
                        <div class="solution-item">
                           <p class="theme-description">Grab the chance of empowering our skilled traders, cutting-edge technology and direct settlement in your custody account.</p>
                           <a href="">Brokerage & Trading</a>
                        </div>
                        <div class="solution-item">
                           <p class="theme-description">Grab extra returns easily with our liquidity and get income solutions, specially made to fulfil your needs.</p>
                           <a href="">Financing & Structured Solutions</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xl-7 col-lg-7 col-md-7 col-sm-6 col-12">
                  <div class="solution-media">
                     <img src="images/screenshot-3.png">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="team-area page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Our Team Is With You, At All Time</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="team-box">
                     <h3 class="theme-title">High-touch Support</h3>
                     <p class="theme-description">DEFINIS staff is always there to help DEFINIS users solve relevant queries.</p>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="team-box">
                     <h3 class="theme-title">Best-in-class Expertise</h3>
                     <p class="theme-description">Team DEFINIS includes experts in respective fields to match the requirements.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-video page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Ready to learn more?</h2>
                     <p>Jump into DEFINIS’ ecosystem by <br>clicking the button below.</p>
                  </div>
                  <div class="service-btn text-center mt-4">
                     <a class="btn-main get-started" href="https://app.definis.io/register">GET STARTED</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>