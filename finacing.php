<?php include('header.php'); ?>
      <section class="hero-service">
         <div class="bg-media">
            <img src="images/slider-dot.png" alt="">
         </div>
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                  <div class="hero-service-info">
                     <div class="page-title text-left">
                        <h4>DEFINIS Finance</h4>
                        <h1>The Digital Future Of Financing</h1>
                        <p>Financing digital assets on DEFINIS has a promising future that may attract several investors.</p>
                     </div>
                     <div class="hero-btn">
                        <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                  <div class="hero-service-media text-center">
                     <!-- <img src="images/future.png" alt=""> -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-grid page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>The Next-Gen Financing on DEFINIS</h2>
                     <p>Investors can use DEFINIS for Financing digital assets. By Financing, investors will have the chance of earning high benefits or profits in terms of digital assets such as crypto.</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/finance/straightforward-processes.png">
                     </div>
                     <h3 class="theme-title">Straightforward processes</h3>
                     <p class="theme-description">Transaction processes and other respective processes are transparent and straightforward on DEFINIS to avoid inconsistencies.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/finance/compliant-crypto-financing.png">
                     </div>
                     <h3 class="theme-title">Compliant crypto financing</h3>
                     <p class="theme-description">Crypto financing on DEFINIS is flexible, which means financing follows AML cryptocurrency regulations, which prevents fraud.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/finance/stablecoin-lending-and-borrowing.png">
                     </div>
                     <h3 class="theme-title">stablecoin lending and borrowing</h3>
                     <p class="theme-description">Lending and borrowing can take place between two users as DEFINIS allows users to do the same with significant returns.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/finance/strategic-loan-structuring.png">
                     </div>
                     <h3 class="theme-title">Strategic loan structuring</h3>
                     <p class="theme-description">Loan structure can be varied by various factors, including the condition of borrowing requests and the client's risk factor.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/finance/integrated-services.png">
                     </div>
                     <h3 class="theme-title">Integrated services</h3>
                     <p class="theme-description">DEFINIS has integrated various services related to digital assets like custody, stablecoin payments, settlements etc.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/finance/broad-and-expanding-asset-support.png">
                     </div>
                     <h3 class="theme-title">Broad and expanding asset support</h3>
                     <p class="theme-description">DEFINIS’ support staff is widely spread across the world and we’re working on expanding the staff for users’ convenience.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-video page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Ready to learn more?</h2>
                     <p>Jump into DEFINIS’ ecosystem by <br>clicking the button below.</p>
                  </div>
                  <div class="service-btn text-center mt-4">
                     <a class="btn-main get-started" href="https://app.definis.io/register">GET STARTED</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>