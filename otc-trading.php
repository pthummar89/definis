<?php include('header.php'); ?>
      <section class="hero-service">
         <div class="bg-media">
            <img src="images/slider-dot.png" alt="">
         </div>
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                  <div class="hero-service-info">
                     <div class="page-title text-left">
                        <h4>DEFINIS OTC Trading</h4>
                        <h1>The Digital Future of OTC Trading</h1>
                        <p>DEFINIS has the feature of OTC trading to attract several traders.</p>
                     </div>
                     <div class="hero-btn">
                        <a href="https://app.definis.io/register" class="btn-main">Get Started</a>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                  <div class="hero-service-media text-center">
                     <!-- <img src="images/future.png" alt=""> -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-grid page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>The New Heights of OTC Trading</h2>
                     <p>DEFINIS lets traders trade directly with another trader without the supervision of an exchange. OTC trading has the potential of facilitating liquidity, providing transparency, and maintaining the current market price.</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/otc-trading/broad-liquidity.png">
                     </div>
                     <h3 class="theme-title">Broad liquidity</h3>
                     <p class="theme-description">Digital assets on DEFINIS can be easily converted into cash, thanks to its unbeatable liquidity.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/otc-trading/best-execution.png">
                     </div>
                     <h3 class="theme-title">Best execution</h3>
                     <p class="theme-description">Execution services tend to strengthen traders’ global trading capability. DEFINIS provides the best execution services to its users.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/otc-trading/365-access.png">
                     </div>
                     <h3 class="theme-title">24/7/365 Access</h3>
                     <p class="theme-description">The digital asset platform of DEFINIS is accessible 24x7. The platform is integrated online to make it available anytime, anywhere.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/otc-trading/no-pre-funding.png">
                     </div>
                     <h3 class="theme-title">No pre-funding</h3>
                     <p class="theme-description">Pre-funding of user accounts isn’t necessary at all. Users can continue using the DEFINIS platform without pre-funding.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/otc-trading/fast-automatic-settlement.png">
                     </div>
                     <h3 class="theme-title">Fast, automatic settlement</h3>
                     <p class="theme-description">Settlement on DEFINIS makes the creation and maintenance of control accounts simple and routine for DEFINIS users.</p>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 service-item-box">
                  <div class="service-item">
                     <div class="service-ico">
                        <img src="images/icon/otc-trading/transparent-pricing.png">
                     </div>
                     <h3 class="theme-title">Transparent pricing</h3>
                     <p class="theme-description">The pricing of digital assets on DEFINIS doesn’t contain any hidden charges. DEFINIS maintains transparency in terms of pricing.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="service-video page-background">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Ready to learn more?</h2>
                     <p>Jump into DEFINIS’ ecosystem by <br>clicking the button below.</p>
                  </div>
                  <div class="service-btn text-center mt-4">
                     <a class="btn-main get-started" href="https://app.definis.io/register">GET STARTED</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="partner-area page-paddings">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="page-title">
                     <h2>Trusted Partners</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="partner-main">
                     <div class="partner-slider">
                        <div class="partner-slider-main">
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                           <div class="partner-item">
                              <img src="images/partner/partner1.png" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('footer.php'); ?>